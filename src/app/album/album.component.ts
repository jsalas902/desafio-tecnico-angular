import { Component, OnInit, ViewChild } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { MatDialog, MatPaginator } from '@angular/material';
import {MatSort} from '@angular/material/sort';
import { Album } from '@models/album';
import { AlbumService } from '@services/album.service';
import { OpenDialogAlbumComponent } from './components/open-dialog-album/open-dialog-album.component';
import { NotifierService } from '@services/notifier.service';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.css']
})
export class AlbumComponent implements OnInit {
  displayedColumns: string[] = ['id', 'userId', 'title', 'action'];
  dataSource!:  MatTableDataSource<Album>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private albumServices: AlbumService,public dialog: MatDialog, private notifierService: NotifierService) { }
  
  ngOnInit() {
    this.getAlbums();
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if(this.dataSource.paginator){
      this.dataSource.paginator.firstPage();
    }
  }
  getAlbums(){
    this.albumServices.getAll().subscribe({
      next:(res)=>{
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error:(err)=>{
        this.notifierService.showNotification('Error while read the data from server!', 'OK', 'error');
      }
    })
  }
  openDialog(action,obj) {
    obj.action = action;
    const dialogRef = this.dialog.open(OpenDialogAlbumComponent, {
      width: '30%',
      data:obj
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result.event == 'Add'){
        this.addRowData(result.data);
      }else if(result.event == 'Update'){
        this.updateRowData(result.data);
      }else if(result.event == 'Delete'){
        this.deleteRowData(result.data);
      }
    });
  }
  addRowData(row_obj){
    this.dataSource.data.push(row_obj);
    this.dataSource._updateChangeSubscription();
    
  }
  updateRowData(row_obj){
    this.dataSource.data.filter((value,key)=>{
      if(value.id == row_obj.id){
        value.title = row_obj.title;
        value.userId = row_obj.userId;
      }
      return true;
    });
  }
  deleteRowData(row_obj){
    this.dataSource.data = this.dataSource.data.filter((value,key)=>{
      return value.id != row_obj.id;
    });
  }
}
