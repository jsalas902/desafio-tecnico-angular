import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlbumService } from '@services/album.service';
import { UserService } from '@services/user.service';
import {AlbumValidator} from '../validation/album.validator'
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Album } from '@models/index';
import { NotifierService } from '@services/notifier.service';
@Component({
  selector: 'app-open-dialog-album',
  templateUrl: './open-dialog-album.component.html',
  styleUrls: ['./open-dialog-album.component.css']
})
export class OpenDialogAlbumComponent implements OnInit {
  albumForm !: FormGroup;
  album_validation_messages = AlbumValidator.ALBUM_VALIDATION_MESSAGES;
  action:string;
  local_data:any;
  constructor(
    public userServices: UserService,
    private albumServices: AlbumService,
    private notifierService: NotifierService,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public editData: Album,
    private dialogRef: MatDialogRef<OpenDialogAlbumComponent> ) { 
      this.local_data = {...editData};
      this.action = this.local_data.action;
  }

  ngOnInit() {
    this.albumForm = this.formBuilder.group({
      title: ['',[Validators.required]],
      userId: ['',[Validators.required]],
    });
    if(this.editData) this.patchValueAlbum();
  }
  get f() { return this.albumForm.controls; }
  doAction(){
    if (this.albumForm.invalid)return;  
    if(this.action == 'Add'){
      this.create(); 
    }else if(this.action == 'Update'){
      this.edit();
    }else if(this.action == 'Delete'){
      this.delete();
    }
  }
  create(){
    this.albumServices.add(this.albumForm.value).subscribe({
      next:(res)=>{
        this.notifierService.showNotification('Element added successfully', 'OK', 'success');
        this.dialogRef.close({event:this.action,data:res});
      },
      error:(err)=>{
        this.notifierService.showNotification('Error while adding the data from server!', 'OK', 'error');
      }
    })
  }
  edit(){
    this.albumServices.update(this.albumForm.value,this.editData.id).subscribe({
      next:(res)=>{
        this.notifierService.showNotification('Element updating successfully', 'OK', 'success');
        this.dialogRef.close({event:this.action,data:res});
      },
      error:(err)=>{
        this.notifierService.showNotification('Error while updating the data from server!', 'OK', 'error');
      }
    })
  }
  delete(){
    this.albumServices.delete(this.editData.id).subscribe({
      next:(res)=>{
        this.notifierService.showNotification('Element deleted successfully', 'OK', 'success');
        this.dialogRef.close({event:this.action,data:this.editData});
      },
      error:(err)=>{
        this.notifierService.showNotification('Error while deleted the data from server!', 'OK', 'error');
      }
    })
  }
  patchValueAlbum(){
    this.albumForm.patchValue({
      id: this.editData.id,
      title: this.editData.title,
      userId: this.editData.userId,
    });
  }
}
