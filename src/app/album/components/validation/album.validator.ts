export class AlbumValidator {
    public static ALBUM_VALIDATION_MESSAGES = {
        'title': [
            { type: 'required', message: 'Title is required' },
        ],
        'userId': [
            { type: 'required', message: 'User is required' },
        ]
    }
}