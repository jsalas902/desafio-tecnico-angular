import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MaterialModule } from '@app/material.module';
import { AlbumComponent } from './album.component';
import { RouterModule } from '@angular/router';
import { OpenDialogAlbumComponent } from './components/open-dialog-album/open-dialog-album.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UtilModule } from '@app/util/util.module';


@NgModule({
  declarations: [AlbumComponent, OpenDialogAlbumComponent],
  imports: [
    CommonModule,
    MaterialModule,
    UtilModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: AlbumComponent
      }
    ]),
  ],
  entryComponents: [ OpenDialogAlbumComponent]
})
export class AlbumModule { }
