import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Post } from '@models/post';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  private url = environment.apiUrl + `/posts`;
  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<Post[]>(this.url);
  }
  add(data:Post) {
    return this.http.post<Post>(this.url,data);
  }
  update(data:Post, id: number) {
    return this.http.put<Post>(`${this.url}/${id}`,data);
  }
  delete(id: number) {
    return this.http.delete<Post>(`${this.url}/${id}`);
  }
}
