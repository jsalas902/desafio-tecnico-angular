import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Album } from '@models/index';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AlbumService {
  private url = environment.apiUrl + `/albums`;
  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<Album[]>(this.url);
  }
  add(data:Album) {
    return this.http.post<Album>(this.url,data);
  }
  update(data:Album, id: number) {
    return this.http.put<Album>(`${this.url}/${id}`,data);
  }
  delete(id: number) {
    return this.http.delete<Album>(`${this.url}/${id}`);
  }
}
