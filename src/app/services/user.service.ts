import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '@models/user';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  userList: User[] = [];
  users = new BehaviorSubject<User[]>([]);
  users$ = this.users.asObservable();

  constructor(private http: HttpClient) {
    this.users$.subscribe(v=> this.userList = v)
  }

  getAll() {
    const url = environment.apiUrl + `/users`;
    return this.http.get<User[]>(url);     
  }
  currentValue() {
    return this.userList;
  }
}
