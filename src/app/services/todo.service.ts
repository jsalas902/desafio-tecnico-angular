import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Todo } from '@models/index';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class TodoService {
  private url = environment.apiUrl + `/todos`;
  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<Todo[]>(this.url);
  }
  add(data:Todo) {
    return this.http.post<Todo>(this.url,data);
  }
  update(data:Todo, id: number) {
    return this.http.put<Todo>(`${this.url}/${id}`,data);
  }
  delete(id: number) {
    return this.http.delete<Todo>(`${this.url}/${id}`);
  }
}
