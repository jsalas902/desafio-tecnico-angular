import { Routes, RouterModule } from '@angular/router';
import { DefaultLayoutComponent } from '@layout/default-layout/default-layout.component';

const appRoutes: Routes = [
	{
        path: '',
        component: DefaultLayoutComponent, // Default layout
        children: [
			{
				path: '', loadChildren: () => import('@app/todo/todo.module').then(m => m.TodoModule)
			},
			{
				path: 'posts', loadChildren: () => import('@app/post/post.module').then(m => m.PostModule)
			},
			{
				path: 'albums', loadChildren: () => import('@app/album/album.module').then(m => m.AlbumModule)
			}
	  	],
	},
	// otherwise redirect to todos
	{ path: '**', redirectTo: '/' }
];

export const routing = RouterModule.forRoot(appRoutes, { scrollOffset: [0, 0], scrollPositionRestoration: 'top' });
