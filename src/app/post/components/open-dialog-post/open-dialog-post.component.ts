import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validator, Validators } from '@angular/forms';
import { PostService } from '@services/post.service';
import { UserService } from '@services/user.service';
import {PostValidator} from '../validation/post.validator'
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Post } from '@models/index';
import { NotifierService } from '@services/notifier.service';

@Component({
  selector: 'app-open-dialog-post',
  templateUrl: './open-dialog-post.component.html',
  styleUrls: ['./open-dialog-post.component.css']
})
export class OpenDialogPostComponent implements OnInit {
  postForm !: FormGroup;
  post_validation_messages = PostValidator.POST_VALIDATION_MESSAGES;
  action:string;
  local_data:any;
  constructor(
    public userServices: UserService,
    private postServices: PostService,
    private notifierService: NotifierService,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public editData: Post,
    private dialogRef: MatDialogRef<OpenDialogPostComponent> ) { 
      this.local_data = {...editData};
      this.action = this.local_data.action;
  }

  ngOnInit() {
    this.postForm = this.formBuilder.group({
      title: ['',[Validators.required]],
      body: ['',[Validators.required, Validators.minLength(5), Validators.maxLength(250)]],
      userId: ['',[Validators.required]],
    });
    if(this.editData) this.patchValuePost();
  }
  get f() { return this.postForm.controls; }
  doAction(){
    if (this.postForm.invalid)return;  
    if(this.action == 'Add'){
      this.create(); 
    }else if(this.action == 'Update'){
      this.edit();
    }else if(this.action == 'Delete'){
      this.delete();
    }
  }
  create(){
    this.postServices.add(this.postForm.value).subscribe({
      next:(res)=>{
        this.notifierService.showNotification('Element added successfully', 'OK', 'success');
        this.dialogRef.close({event:this.action,data:res});
      },
      error:(err)=>{
        this.notifierService.showNotification('Error while adding the data from server!', 'OK', 'error');
      }
    })
  }
  edit(){
    this.postServices.update(this.postForm.value,this.editData.id).subscribe({
      next:(res)=>{
        this.notifierService.showNotification('Element updating successfully', 'OK', 'success');
        this.dialogRef.close({event:this.action,data:res});
      },
      error:(err)=>{
        this.notifierService.showNotification('Error while updating the data from server!', 'OK', 'error');
      }
    })
  }
  delete(){
    this.postServices.delete(this.editData.id).subscribe({
      next:(res)=>{
        this.notifierService.showNotification('Element deleted successfully', 'OK', 'success');
        this.dialogRef.close({event:this.action,data:this.editData});
      },
      error:(err)=>{
        this.notifierService.showNotification('Error while deleted the data from server!', 'OK', 'error');
      }
    })
  }
  patchValuePost(){
    this.postForm.patchValue({
      id: this.editData.id,
      title: this.editData.title,
      body: this.editData.body,
      userId: this.editData.userId,
    });
  }
}
