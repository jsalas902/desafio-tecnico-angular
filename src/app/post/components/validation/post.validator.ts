export class PostValidator {
    public static POST_VALIDATION_MESSAGES = {
        'title': [
            { type: 'required', message: 'Title is required' },
        ],
        'body': [
            { type: 'required', message: 'Body is required' },
            { type: 'minlength', message: 'The body cannot be less than 5 characters' },
            { type: 'maxlength', message: 'Body cannot be more than 250 characters long' }
        ],
        'userId': [
            { type: 'required', message: 'User is required' },
        ]
    }
}