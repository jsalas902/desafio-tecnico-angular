import { Component, OnInit, ViewChild } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { MatDialog, MatPaginator } from '@angular/material';
import {MatSort} from '@angular/material/sort';
import { Post } from '@models/post';
import { PostService } from '@services/post.service';
import { OpenDialogPostComponent } from './components/open-dialog-post/open-dialog-post.component';
import { NotifierService } from '@services/notifier.service';
@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  displayedColumns: string[] = ['id', 'userId', 'title', 'body', 'action'];
  dataSource!:  MatTableDataSource<Post>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private postServices: PostService,public dialog: MatDialog, private notifierService: NotifierService) { }
  
  ngOnInit() {
    this.getPosts();
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if(this.dataSource.paginator){
      this.dataSource.paginator.firstPage();
    }
  }
  getPosts(){
    this.postServices.getAll().subscribe({
      next:(res)=>{
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error:(err)=>{
        this.notifierService.showNotification('Error while read the data from server!', 'OK', 'error');
      }
    })
  }
  openDialog(action,obj) {
    obj.action = action;
    const dialogRef = this.dialog.open(OpenDialogPostComponent, {
      width: '30%',
      data:obj
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result.event == 'Add'){
        this.addRowData(result.data);
      }else if(result.event == 'Update'){
        this.updateRowData(result.data);
      }else if(result.event == 'Delete'){
        this.deleteRowData(result.data);
      }
    });
  }
  addRowData(row_obj){
    this.dataSource.data.push(row_obj);
    this.dataSource._updateChangeSubscription();
    
  }
  updateRowData(row_obj){
    this.dataSource.data.filter((value,key)=>{
      if(value.id == row_obj.id){
        value.title = row_obj.title;
        value.body = row_obj.body;
        value.userId = row_obj.userId;
      }
      return true;
    });
  }
  deleteRowData(row_obj){
    this.dataSource.data = this.dataSource.data.filter((value,key)=>{
      return value.id != row_obj.id;
    });
  }

}
