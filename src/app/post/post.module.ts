import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {MaterialModule } from '@app/material.module';
import { PostComponent } from './post.component';
import { OpenDialogPostComponent } from './components/open-dialog-post/open-dialog-post.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UtilModule } from '@app/util/util.module';



@NgModule({
  declarations: [PostComponent, OpenDialogPostComponent],
  imports: [
    CommonModule,
    MaterialModule,
    UtilModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: PostComponent
      }
    ]),
  ],
  entryComponents: [ OpenDialogPostComponent]
})
export class PostModule { }
