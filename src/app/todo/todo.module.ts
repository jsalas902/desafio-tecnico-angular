import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@app/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TodoComponent } from './todo.component';
import { OpenDialogTodoComponent } from './components/open-dialog-todo/open-dialog-todo.component';
import { UtilModule } from '@app/util/util.module';


@NgModule({
  declarations: [TodoComponent, OpenDialogTodoComponent],
  imports: [
    CommonModule,
    MaterialModule,
    UtilModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: TodoComponent
      }
    ]),
  ],
  entryComponents: [OpenDialogTodoComponent]
})
export class TodoModule { }
