export class TodoValidator {
    public static TODO_VALIDATION_MESSAGES = {
        'title': [
            { type: 'required', message: 'Title is required' },
        ],
        'completed': [
            { type: 'required', message: 'Completed is required' },
        ],
        'userId': [
            { type: 'required', message: 'User is required' },
        ]
    }
}