import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TodoService } from '@services/todo.service';
import { UserService } from '@services/user.service';
import { TodoValidator } from '../validation/todo.validator'
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Todo } from '@models/index';
import { NotifierService } from '@services/notifier.service';

@Component({
  selector: 'app-open-dialog-todo',
  templateUrl: './open-dialog-todo.component.html',
  styleUrls: ['./open-dialog-todo.component.css']
})
export class OpenDialogTodoComponent implements OnInit {
  todoForm !: FormGroup;
  todo_validation_messages = TodoValidator.TODO_VALIDATION_MESSAGES;
  action:string;
  local_data:any;
  constructor(
    public userServices: UserService,
    private todoServices: TodoService,
    private notifierService: NotifierService,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public editData: Todo,
    private dialogRef: MatDialogRef<OpenDialogTodoComponent> ) { 
      this.local_data = {...editData};
      this.action = this.local_data.action;
  }

  ngOnInit() {
    this.todoForm = this.formBuilder.group({
      title: ['',[Validators.required]],
      completed: ['',[Validators.required]],
      userId: ['',[Validators.required]],
    });
    if(this.editData) this.patchValueTodo();
  }
  get f() { return this.todoForm.controls; }
  doAction(){
    if (this.todoForm.invalid)return;  
    if(this.action == 'Add'){
      this.create(); 
    }else if(this.action == 'Update'){
      this.edit();
    }else if(this.action == 'Delete'){
      this.delete();
    }
  }
  create(){
    this.todoServices.add(this.todoForm.value).subscribe({
      next:(res)=>{
        this.notifierService.showNotification('Element added successfully', 'OK', 'success');
        this.dialogRef.close({event:this.action,data:res});
      },
      error:(err)=>{
        this.notifierService.showNotification('Error while adding the data from server!', 'OK', 'error');
      }
    })
  }
  edit(){
    this.todoServices.update(this.todoForm.value,this.editData.id).subscribe({
      next:(res)=>{
        this.notifierService.showNotification('Element updating successfully', 'OK', 'success');
        this.dialogRef.close({event:this.action,data:res});
      },
      error:(err)=>{
        this.notifierService.showNotification('Error while updating the data from server!', 'OK', 'error');
      }
    })
  }
  delete(){
    this.todoServices.delete(this.editData.id).subscribe({
      next:(res)=>{
        this.notifierService.showNotification('Element deleted successfully', 'OK', 'success');
        this.dialogRef.close({event:this.action,data:this.editData});
      },
      error:(err)=>{
        this.notifierService.showNotification('Error while deleted the data from server!', 'OK', 'error');
      }
    })
  }
  patchValueTodo(){
    this.todoForm.patchValue({
      id: this.editData.id,
      title: this.editData.title,
      completed: this.editData.completed,
      userId: this.editData.userId,
    });
  }
}
