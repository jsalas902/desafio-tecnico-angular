import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav';
@Component({
  selector: 'app-default-layout',
  templateUrl: './default-layout.component.html',
  styleUrls: ['./default-layout.component.css']
})
export class DefaultLayoutComponent implements OnInit {
  @ViewChild('drawer', { static: true }) public drawer!: MatDrawer;
  constructor() { }

  ngOnInit() {
  }
  onToggle() {
    this.drawer.toggle();
  }
}
