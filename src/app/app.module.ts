// Module
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MaterialModule } from './material.module';
import { RouterModule } from '@angular/router';

// Routing
import { routing } from '@app/app.routing';

// Components
import { AppComponent } from './app.component';
import { HeaderComponent } from '@layout/header/header.component';
import { DefaultLayoutComponent } from '@layout/default-layout/default-layout.component';
import { MenuComponent } from './_layout/menu/menu.component';
import { NotifierComponent } from './notifier/notifier.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DefaultLayoutComponent,
    MenuComponent,
    NotifierComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MaterialModule,
    routing,
    BrowserAnimationsModule
  ],
  entryComponents: [NotifierComponent],
  providers: [],
  bootstrap: [AppComponent],
  exports: [RouterModule]

})
export class AppModule { }
