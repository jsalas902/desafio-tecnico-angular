import { Component } from '@angular/core';
import { UserService } from '@services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'desafio-tecnico-angular';
  constructor(private userServices: UserService) { 
    this.userServices.getAll().subscribe({
      next:(res)=>{
        this.userServices.users.next(res);
      },
      error:(err)=> {
        console.log(err)
      }
    })
  }
  
}
