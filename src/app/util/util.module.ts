import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExtractUserNamePipe } from './extract-user-name.pipe';



@NgModule({
  declarations: [ExtractUserNamePipe],
  imports: [
    CommonModule
  ],
  exports: [ExtractUserNamePipe]
})
export class UtilModule { }
