import { Pipe, PipeTransform } from '@angular/core';
import { UserService } from '@services/user.service';

@Pipe({
  name: 'extractUserName'
})
export class ExtractUserNamePipe implements PipeTransform {
  constructor(private userServices: UserService) {}
  transform(id: number): string {
    // search objet user
    const user = this.userServices.users.getValue().find(el => el.id == id);
    return user.name;
  }
}
